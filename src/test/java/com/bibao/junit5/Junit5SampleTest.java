package com.bibao.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Description;

public class Junit5SampleTest {
	@Test
	@Description("Test case for equals")
	public void testEquals() {
		String x1 = "abc";
		String x2 = x1 + "def";
		Assertions.assertEquals("abcdef", x2);
		Assertions.assertNotEquals("abcd", x1);
	}
	
	@Test
	@Description("Test case for true/false")
	public void testTrueFalse() {
		Assertions.assertTrue(5>3);
		Assertions.assertFalse(5<3);
	}
	
	@Test
	@Description("Test case for same/not same")
	public void testSame() {
		String x1 = "abc";
		String x2 = "abc";
		String x3 = new String("abc");
		Assertions.assertSame(x1, x2);
		Assertions.assertNotSame(x1, x3);
	}
	
	@Test
	@Description("Test case for throws")
	public void testThrow() {
		Assertions.assertThrows(NumberFormatException.class, () -> Integer.parseInt("abc"));
		String x = null;
		Assertions.assertThrows(NullPointerException.class, () -> x.toUpperCase());
		int[] array = new int[5];
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> array[6] = 10);
	}
}
